## My Personal Git
```txt
This is my personal git service where i host all of my projects.
```

## Want one for you?

clone this repository and start the server with
```
node index
```

Now make a clone request to any repository at ** http://host:port/my-repo.git **
If the repo exists this will clone the repo otherwise 
it will create a repo for you and then you can clone and start your next big project
in the empty repository.


