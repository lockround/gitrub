var http = require('http');
var spawn = require('child_process').spawn;
var path = require('path');
var backend = require('git-http-backend');
var zlib = require('zlib');
var fs = require('fs');
var cmd = require('node-cmd');

var server = http.createServer(function (req, res) {
    console.log(req.method);
    var repo = req.url.split('/')[1];
    var dir = path.join(__dirname, 'repos', repo);
    if(fs.existsSync(dir)) {
        console.log('exists');
    } else{
        cmd.get(
        `
            git init repos/${repo} --bare -q
            cd repos
            ls
        `,
        function(err, data, stderr){
            if (!err) {
               console.log('the server contains these repos :\n\n',data)
            } else {
               console.log('error', err)
            }
 
        }
    );
    }
    var reqStream = req.headers['content-encoding'] == 'gzip' ? req.pipe(zlib.createGunzip()) : req;
    
    reqStream.pipe(backend(req.url, function (err, service) {
        if (err) return res.end(err + '\n');
        
        res.setHeader('content-type', service.type);
        console.log(service.action, repo, service.fields);
        
        var ps = spawn(service.cmd, service.args.concat(dir));
        ps.stdout.pipe(service.createStream()).pipe(ps.stdin);
        
        })).pipe(res);
});
server.listen(process.env.PORT,()=>console.log(`server on port ${process.env.PORT}`));